﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FengShuiNumber.Model
{
    public class FengShui
    {
        /// <summary>
        /// ID
        /// </summary>
        public long  Id { get; set; }
        /// <summary>
        /// Phone Number
        /// </summary>
        public string Numbers { get; set; }
        /// <summary>
        /// Khoa ngoai lien quan toi nha mang
        /// </summary>
        public long ProviderId { get; set; }
        public Provider provider { get; set; }
    }
}
