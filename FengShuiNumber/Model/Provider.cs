﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FengShuiNumber.Model
{
    public class Provider
    {
        public long Id { get; set; }
        /// <summary>
        /// Name network provider
        /// </summary>
        public string Name { get; set; }
        public virtual ICollection<FengShui> FengShuis { get; set; }
    }
}
