﻿
using FengShuiNumber.Model;
using Microsoft.EntityFrameworkCore;

namespace FengShuiNumber.DBContext
{
    public class FSContext: DbContext
    {
        public DbSet<FengShui> FengShui { get; set; }
        public DbSet<Provider> Provider { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseSqlServer("Server=.\\SQLEXPRESS;Database=EFCore-SchoolDB;Trusted_Connection=True");
            //https://www.entityframeworktutorial.net/efcore/entity-framework-core-console-application.aspx
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<FengShui>()
                .HasOne<Provider>(s => s.provider)
                .WithMany(g => g.FengShuis)
                .HasForeignKey(s => s.ProviderId);
        }

    }
}
